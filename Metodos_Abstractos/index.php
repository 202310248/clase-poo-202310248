<?php 
 //clase abstracta #1 
abstract class Transporte{
 //Metodo abstracto con acceso public
    abstract public function mantenimiento();
}
//Clase heredada #2 Avion 
class Avion extends Transporte{
 //Metodo heredado de la clase Transporte con acceso public.
    public function mantenimiento(){
        //Esto se mostrara en pantalla y son las actividades a realizar del mantenimiento Avion.
        echo "<b>Mantenimiento Avion:</b> <br />
        Revision de alas. <br />
        Revision de estado de neumaticos. <br />
        Revision del nivel de aceite. <br />
        Revision general de estructura. <br />
        Revision de ruedas y frenos. <br />
        Revision de motores. <br />";
    }

}
//Clase heredada #3 Automovil 
class Automovil extends Transporte{
    //Metodo heredado
    public function mantenimiento(){
        echo "<b>Mantenimiento Automovil:</b> <br />
        Cambios de filtro y aceite. <br />
        Revision de frenos. <br />
        Revision de luces. <br />
        Revision de neumaticos. <br />
        Revision de amortiguadores. <br />
        Revision de anticongelante. <br />
        Revision de oxigeno. <br />
        Revision de bujias. <br />
        Limpieza y encerado. <br />";
    }
}
//Clase heredada #4 Tanque
class Tanque extends Transporte{
    //Metodo heredado
    public function mantenimiento(){
        echo "<b>Mantenimiento Tanque:</b> <br />
        Revision de Cadenas de metal o pistas. <br />
        Revision de chasis y cambio si presenta daños. <br />
        Revision de luces. <br />
        Revision de cargamento de armas. <br />
        Carga de equipo de armamento. <br />
        Revision de engrane delantero. <br />
        Revision de volante y pedales. <br />";
    }
} 
//Clase heredada #5 Motocicleta
class Motocicleta extends Transporte{
    //Metodo heredado
    public function mantenimiento(){
        echo "<b>Mantenimiento Motocicleta:</b> <br />
        Revision del nivel de aceite. <br />
        Revision de llantas. <br />
        Revision de luces. <br />
        Revision Frenos y cadenas. <br />";
    }
}
//instanciaciones de las clases y referencias.
$obj = new Avion();
$obj->mantenimiento();
$obj1 = new Automovil();
$obj1->mantenimiento();
$obj2 = new Tanque();
$obj2->mantenimiento();
$obj3 = new Motocicleta();
$obj3->mantenimiento();

?>