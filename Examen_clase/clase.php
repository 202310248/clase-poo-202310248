<?php 
 //nombre de la clase
    class Veterinaria{

 //Atributo de la clase "Veterinaria"
    private $costototal = "";
    
    public $vacunacion = 250;
    public $desparasitacion = 100;

 //Añadimos un constructor con un parametro de valor "$costototal" 
    function __construct($costototal){

        $this->costototal = $costototal;
    }

 //Metodo "costototal" con parametros a y b
    private function costototal($vacunacion,$desparasitacion){
        return $vacunacion + $desparasitacion;
    }
 //Metodos total con parametros nule
    public function total(){
        return $this->costototal;
    }
}
?>