<?php

//Clase Operaciones
class Operaciones{

    //Atributo Operacion a Realizar
    private $OperacionRealizar = "";
    
    public function __construct ($OperacionRealizar) {
        $this->OperacionRealizar = $OperacionRealizar;
    }

    //Metodo Suma con parametros a y b
    public function Suma($a,$b){
        return $a + $b;
    }

    //Metodo Resta con parametros a y b
    public function Resta($a,$b){
        return $a - $b;
    }

    //Metodo Division con parametros a y b
    public function Division($a,$b){
        return $a/$b;
    }

    //Metodo Multiplicacion con parametros a y b
    public function Multiplicacion($a,$b){
        return $a*$b;
    }


    //Metodo que ejecuta los metodos en base a la propiedad $OperacionRealizar
    public function ResultadoOperacion($a,$b){


        switch ($this->OperacionRealizar) {
            case 'suma':
                return $this->Suma($a,$b);
                break;
            case 'resta':
                return $this->Resta($a,$b);
                break;
            case 'division':
                return $this->Division($a,$b);
                break;
            case 'multiplicacion':
                return $this->Multiplicacion($a,$b);
                break;

            default:
                return 'Operacion a Realizar no Definida';
                break;               
        }

    }






}


?>