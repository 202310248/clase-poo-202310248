<?php
//nombre de la clase
class Peluqueria{

//Atributo de la clase "Peluqueria"
    private $precio;

 //Añadimos un constructor con un parametro de valor "$precio" 
    function __construct($precio){

        $this->precio = $precio;
    }
 //Metodos preciocorte con parametros nule
    public function preciocorte(){
        return $this->precio;
    }
}

?>