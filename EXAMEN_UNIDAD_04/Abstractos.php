<?php 
 //clase abstracta #1 
abstract class Fruteria{
 //Metodo abstracto con acceso public
    abstract public function inventario();
}
//Clase heredada #2 Frutas 
class Frutas extends Fruteria{
 //Metodo heredado de la clase Fruteria con acceso public.
    public function inventario(){
        //Esto se mostrara en pantalla y es el inventario de Frutas
        echo "<b>Inventario de Frutas:</b> <br />
        Manzanas. <br />
        Sandias. <br />
        Platanos. <br />
        Naranjas. <br />
        Melones. <br />
        Peras. <br />";
    }

}
//Clase heredada #3 Verduras 
class Verduras extends Fruteria{
    //Metodo heredado
    public function inventario(){
        echo "<b>Inventario de Verduras:</b> <br />
        Lechuga. <br />
        Tomate. <br />
        Cebolla. <br />
        Chile Jalapeño. <br />
        Zanahoria. <br />
        Papas. <br />
        Brocoli. <br />";
    }
}

//instanciaciones de las clases y referencias.
$obj = new Frutas();
$obj->inventario();
$obj1 = new Verduras();
$obj1->inventario();


?>