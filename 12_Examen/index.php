<?php 

 //nombre de la primer clase
 class Violin{
    //Atributo de la primer clase
    public $cuerdas = 4;
    private $clavijas = 4;

 //Añadimos un constructor  
    function __construct($cuerdas){

        $this->cuerdas = $cuerdas;
    }

 //Metodo 1
    public function melodias(){
        return $this->cuerdas;
    }
 //Metodo 2
    private function notastotales(){
        echo "este metodo no puede ser accedido";

    }
 //Metodo 3
    protected function  afinacion(){
        return $this->clavijas;

    }

    function __destruct(){
        $this->cuerdas = null;
        echo "Este es el destructor";
    }
 }

 //Clase dos
  class Ukulele extends Violin{

 //constructor
    function __construct($cuerdas){

        $this->cuerdas = $cuerdas;
    }
 //metodo heredado
    public function AccesoMetodo1(){
        $this->melodias();
    }
      
 //metodo 2
    private function  acordes(){
        echo "No se puede acceder a este metodo";
     
    }
    function __destruct(){
        $this->cuerdas = null;
        echo "Este es el destructor";
    }

  }
   

  $obj = new Violin;
  $obj->melodias();
  $obj2 = new Ukulele;
  $obj->AccesoMetodo1();



?>