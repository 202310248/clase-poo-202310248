<?php 
#Nombre de la clase
class Operaciones{
    #metodos y propiedades  
    public function suma($a,$b){
        $resultado=0;
        $n= func_num_args();
        $parametros= func_get_args();
        for ($i=0; $i < $n; $i++) { 
            $resultado+=$parametros[$i];
            
        }
        return $resultado;
    }
    public function calificacionPracticas(){
        $resultado =0;
        $n = func_num_args();
        $parametros = func_get_args();
        for ($i=0; $i < $n; $i++) { 
            $resultado=$parametros[$i]+$resultado;
        }
        $operacion= $resultado / $n;
        return $operacion;
    }

}
#instanciacion de la clase
$obj = new Operaciones();

#Mostrar en pantalla
echo $obj->suma(4,5,2);
echo "<br><br>";
echo $obj->calificacionPracticas(80,90,100);

?>