<?php 

#Clase creada
class Autos{

    #Atributos de la clase con diferentes modificadores de acceso
    private $color = "";
    public $costo = 1000000;
    protected $matricula = 3518230;

    #Metodos de la clase con diferentes modificadores de acceso
    private function diseño($rojo,$negro){
        return $this->color;
    }

    public function precio(){
        return $this->costo;
    }

    protected function numeros(){
        return $this->matricula;
    }

}
#Esta es la instanciacion de la clase Autos
$obj = new Autos();

#Se imprimira en pantalla
echo "El costo del auto es: ".$obj->precio();


?>